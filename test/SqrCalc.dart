import 'dart:math';
import 'dart:io';

main() {
  int a, b, c, d;

  print("Print a:");
  a = int.parse(stdin.readLineSync());

  print("Print b:");
  b = int.parse(stdin.readLineSync());

  print("Print c:");
  c = int.parse(stdin.readLineSync());

  print("Your equation is  $a * x^2 + $b * x + $c = 0");

  d = b*b-4*a*c;

  print("Discriminant:");
  print(d);

  if (d>0) {
    print("x1 = ${(b*(-1)+sqrt(d))/(2*a)}");
    print("x2 = ${(b*(-1)-sqrt(d))/(2*a)}");
  }
  else if (d == 0) {
    print("x = ${(b*(-1))/(2*a)}");
  }
  else {
    print("it's imposible for rational figures");
  }
}