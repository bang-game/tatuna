import 'LinkdList.dart';

main() {
  LinkedList<String> l = LinkedList<String>();
  l.add("Hello");
  l.add("man");
  l.add("how");
  l.add("good");
  l.add("test");
  print(l.toString());
  print(" ");

  l.setAt(2, "data");
  print(l.toString());
  print(" ");

  l.remove("test");
  print(l.toString());
  print(" ");


  l.removeAt(0);
  print(l.toString());
  print(" ");
}