class Node<T> {
  //creates a new item that is a linked list object
  var data;
  var prev;
  var next;

  Node([this.data, this.prev, this.next]);

}

class LinkedList<T> {
  Node head = Node();
  num size;
  Node last = Node();

  LinkedList([this.head, this.size = 0, this.last]);

  add(T data) {
    size++;

    if (head == null) {
      head = Node(data);
      last = head;
    }

    else {
      Node current = Node(data);
      current.prev = last;
      last.next = current;
      last = current;
    }
  }

  remove(T data) {
    Node current = head;

    while (current != null) {
      if (current.data != data) {
        current = current.next;
      }
      else {
        break;
      }
    }
    if (current.next != null && current.prev != null) {
      current.next.prev = current.prev;
      current.prev.next = current.next;
      current.prev = null;
      current.next = null;
    }
    else if (current.next == null){
      last = null;
      last = current.prev;
      last.next = null;
    }
    else {
      head = null;
      head = current.next;
      head.prev = null;
    }
    size--;
  }

  removeAt(int index) {
    Node current;
    int counter;
    if(index > size/2) {
      current = last;
      counter = size - 1;
      while(counter != index) {
        counter--;
        current = current.prev;
      }
      if (current.next != null) {
        current.next.prev = current.prev;
        current.prev.next = current.next;
        current.prev = null;
        current.next = null;
      }
      else {
        last = null;
        last = current.prev;
        last.next = null;
      }
    }
    else {
      current = head;
      counter = 0;
      while(counter != index) {
        counter++;
        current = current.next;
      }
      if (current.prev != null) {
        current.next.prev = current.prev;
        current.prev.next = current.next;
        current.prev = null;
        current.next = null;
      }
      else {
        head = null;
        head = current.next;
        head.prev = null;
      }
    }
    size--;
  }

  setAt(int index, T data) {
    Node current = last;

    if(index == size) {
      current.next = Node();
      current = current.next;
      last = current;
    }
    else {
      int counter = size - 1;
      current.next = Node();
      current.next.data = current.data;
      current.next.prev = current;
      last = current.next;

      while (counter != index) {
        counter--;
        current = current.prev;
        current.next.data = current.data;
        current.next.prev = current;
      }
    }
    current.data = data;
    size++;
  }

  @override
  String toString() {
    String nodesStr = "";
    var current = head;
    for (int i = 0; i < size; i++) {
      nodesStr += current.data.toString();
      if (i != size - 1) {
        nodesStr += ", ";
      }
      current = current.next;
    }
    String str = "[$nodesStr]";
    return str;
  }
}





